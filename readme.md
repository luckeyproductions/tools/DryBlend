![Icon](icon.svg)

# DryBlend

[Blender](https://www.blender.org) to [Dry](https://dry.luckey.games) scene and mesh exporter. The guide can be found [here](guide.md).

## Installation

![install](install_instructions.png)
